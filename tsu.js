/*LICENSE*//*Copyright 2019 Ben Matthews
	Permission is hereby granted, free of charge, to any person obtaining a copy 
	of this software and associated documentation files (the "Software"), to deal 
	in the Software without restriction, including without limitation the rights 
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
	copies of the Software, and to permit persons to whom the Software is 
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in 
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
	THE SOFTWARE.*/
"use strict";

var Tsu = {};
Tsu.init = function(den){
	//== BoilerPlate ==============================================
		let defaultBuffer = {};
		if (window.Buffer != undefined) defaultBuffer = Buffer;
		let {
			triFan, 
			Texture, 
			float, float2, float4, 
			f1tex, f2tex, f4tex,
			i2buf, i3buf,
			f4img, f4vid,
			input,
		} = den;

		let context = {};
	//== Imports ==================================================
		let fs = window.require ? require('fs') : {};
		let test;
	//== Window/Desktop ===========================================
		context.autoLoop = function(f){
			let g = () => {
				requestAnimationFrame(g);
				f();
			}; requestAnimationFrame(g);
		};
	//== Utility functions ========================================
		context.shuffle = function(arr){
			for (let i = 0; i < arr.length; i++){
				let j = Math.floor(Math.random()*i);
				[arr[i],arr[j]] = [arr[j], arr[i]];
			}
			return arr;
		}
	//== File Loading =============================================
		context.imgExts = ["png", "jpg", "jpeg", "gif", "apng", "bmp"];
		context.vidExts = ["mp4", "ogg", "webm"];
		context.audExts = ["mp3", "wav", "ogg"];

		let inArr    = (arr, name) => arr.includes(name.split(".").slice(-1)[0].toLowerCase());
		let isImage  = name => inArr(context.imgExts, name);
		let isVideo  = name => inArr(context.vidExts, name);
		let isAudio  = name => inArr(context.audExts, name);
		let tryCatch      =       f => {try {f()} catch (err){console.warn(err)}}
		let tryCatchAsync = async f => {try {await f()} catch (err){console.warn(err)}}

		context.isImage = isImage;
		context.isAudio = isAudio;

		context.getFileNames = function(path){
			let names = [];
			tryCatch(() => names = fs.readdirSync(path));
			return names;
		}

		context.readJSON = function(path){
			let obj = {};
			tryCatch(() => obj = JSON.parse(fs.readFileSync(path)))
			return obj;
		}

		context.saveJSON = function(obj, path){
			tryCatch(() => {
				let json = JSON.stringify(obj, null, '\t');
				fs.writeFileSync(path, json);
			})
		}

		//loads all the given media files
		context.loadMedia = async function(files){
			let loads = {images:[], images2D:[], videos:[], audio:[]};
			for (let file of files){
				if (isImage(file)){
					let img = new Image();
					img.src = file;
					loads.images2D.push(img);
				}
				if (isImage(file)){let temp = await f4img(file); loads.images.push(temp)}
				if (isVideo(file)){let temp = await f4vid(file); loads.videos.push(temp)}
				if (isAudio(file)){let temp =   new Audio(file); loads.audio .push(temp)}
			}
			return loads
		}

		context.getImages = async function(path){
			let images = [];
			let names = context.getFileNames(path);
			names = names.filter(name => isImage(name));
			for (let name of names){
				let img = await f4img(path + name)
				images.push(img);
			}
			return images;      
		}

		context.getImages2D = function(path){
			let images = [];
			let names = context.getFileNames(path);
			names = names.filter(name => isImage(name));
			names.map(name => {
				let img = new Image();
				img.src = path + name;
				images.push(img);
			})
			return images;
		}

		context.getVideos = async function(path){
			let videos = [];
			let names = context.getFileNames(path);
			names = names.filter(name => isVideo(name));
			for (let name of names){
				let vid = await f4vid(path + name);
				videos.push(vid);
			}
			return videos;
		}

		context.getAudio = function(path){
			let audio = [];
			let names = Tsu.getFileNames(path);
			names = names.filter(name => inArr(context.audExts, name));
			names.map(name => {
				let aud = new Audio(path + name);
				audio.push(aud);
			})
			return audio;
		}

		context.loadFont = async function(name, path){
			let font;
			tryCatchAsync( async() => {
				font = new FontFace(name, `url(${path})`)
				await font.load();
				document.fonts.add(font);
			})
			return font;
		}

		context.saveFile = function(path, data, options={}){fs.writeFileSync(path, data, options)}
	//== GLSL header functions ====================================

		let header = context.header = `
			#define _uv (pixel/res)
			#define _thisUV(tex) texture(tex,_uv)

			#define _getPixel(tex,coord) texture(tex,(coord)/res)
			#define _thisPixel(tex) _thisUV(tex)
			#define _pixelOffset(tex,x,y) _getPixel(tex,pixel+vec2(x,y))
			#define _pixelL(tex) _pixelOffset(tex,-1, 0)
			#define _pixelR(tex) _pixelOffset(tex, 1, 0)
			#define _pixelD(tex) _pixelOffset(tex, 0,-1)
			#define _pixelU(tex) _pixelOffset(tex, 0, 1)

			#define _getTexel(tex,coord) texelFetch(tex,coord,0)
			#define _thisTexel(tex) _getTexel(tex,texel)
			#define _texelOffset(tex,x,y) _getTexel(tex,texel+ivec2(x,y))
			#define _texelL(tex) _texelOffset(tex,-1, 0)
			#define _texelR(tex) _texelOffset(tex, 1, 0)
			#define _texelD(tex) _texelOffset(tex, 0,-1)
			#define _texelU(tex) _texelOffset(tex, 0, 1)

			#define forManhattan(i,j,range) for (int i=-range;i<=range;++i) for (int j=-(range-abs(i));j<=range-abs(i);++j)
			#define forChebyshev(i,j,range) for (int i=-range;i<=range;++i) for (int j=-range;j<=range;++j)

			vec3 _rgb2hsv(vec3 c){
				vec4 K = vec4(0, -1./3., 2./3., -1);
				vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
				vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

				float d = q.x - min(q.w, q.y);
				float e = 1.e-10;
				return vec3(abs(q.z + (q.w - q.y)/(6.*d + e)), d/(q.x + e), q.x);
			}

			vec3 _hsv2rgb(vec3 c){
				vec4 K = vec4(1, 2./3., 1./3., 3.);
				vec3 p = abs(fract(c.xxx + K.xyz)*6. - K.www);
				return c.z*mix(K.xxx, clamp(p - K.xxx, 0., 1.), c.y);
			}

			vec3 _hsvMix(vec3 c1, vec3 c2, float amt){
				return _hsv2rgb(mix(_rgb2hsv(c1), _rgb2hsv(c2), amt));
			}

			vec2 _rotate(vec2 v, float a){
				return mat2(cos(a), -sin(a), sin(a), cos(a))*v;
			}

			vec2 _cosSin(float a){
				return vec2(cos(a), sin(a));
			}

			vec4 _dXdY(sampler2D tex, vec2 pos, vec2 size, float offset){
				vec4 v = vec4(0);
				v += texture(tex, (pos+vec2( offset, 0))/size);
				v += texture(tex, (pos+vec2(-offset, 0))/size);
				v += texture(tex, (pos+vec2(0,  offset))/size);
				v += texture(tex, (pos+vec2(0, -offset))/size);
				return v/4.;
			}
		`

		let hash = `
			// Hash without Sine
			// MIT License...
			/* Copyright (c)2014 David Hoskins.

			Permission is hereby granted, free of charge, to any person obtaining a copy
			of this software and associated documentation files (the "Software"), to deal
			in the Software without restriction, including without limitation the rights
			to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
			copies of the Software, and to permit persons to whom the Software is
			furnished to do so, subject to the following conditions:

			The above copyright notice and this permission notice shall be included in all
			copies or substantial portions of the Software.

			THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
			IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
			FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
			AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
			LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
			OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
			SOFTWARE.*/

			float _hash11(float p){
				p = fract(p * .1031);
				p *= p + 33.33;
				p *= p + p;
				return fract(p);
			}

			float _hash12(vec2 p){
				vec3 p3  = fract(vec3(p.xyx) * .1031);
				p3 += dot(p3, p3.yzx + 33.33);
				return fract((p3.x + p3.y) * p3.z);
			}

			float _hash13(vec3 p3){
				p3  = fract(p3 * .1031);
				p3 += dot(p3, p3.yzx + 33.33);
				return fract((p3.x + p3.y) * p3.z);
			}

			vec2 _hash21(float p){
				vec3 p3 = fract(vec3(p) * vec3(.1031, .1030, .0973));
				p3 += dot(p3, p3.yzx + 33.33);
				return fract((p3.xx+p3.yz)*p3.zy);
			}

			vec2 _hash22(vec2 p){
				vec3 p3 = fract(vec3(p.xyx) * vec3(.1031, .1030, .0973));
				p3 += dot(p3, p3.yzx+33.33);
				return fract((p3.xx+p3.yz)*p3.zy);
			}

			vec2 _hash23(vec3 p3){
				p3 = fract(p3 * vec3(.1031, .1030, .0973));
				p3 += dot(p3, p3.yzx+33.33);
				return fract((p3.xx+p3.yz)*p3.zy);
			}

			vec3 _hash31(float p){
				 vec3 p3 = fract(vec3(p) * vec3(.1031, .1030, .0973));
				 p3 += dot(p3, p3.yzx+33.33);
				 return fract((p3.xxy+p3.yzz)*p3.zyx); 
			}

			vec3 _hash32(vec2 p){
				vec3 p3 = fract(vec3(p.xyx) * vec3(.1031, .1030, .0973));
				p3 += dot(p3, p3.yxz+33.33);
				return fract((p3.xxy+p3.yzz)*p3.zyx);
			}

			vec3 _hash33(vec3 p3){
				p3 = fract(p3 * vec3(.1031, .1030, .0973));
				p3 += dot(p3, p3.yxz+33.33);
				return fract((p3.xxy + p3.yxx)*p3.zyx);
			}

			vec4 _hash41(float p){
				vec4 p4 = fract(vec4(p) * vec4(.1031, .1030, .0973, .1099));
				p4 += dot(p4, p4.wzxy+33.33);
				return fract((p4.xxyz+p4.yzzw)*p4.zywx);
			}

			vec4 _hash42(vec2 p){
				vec4 p4 = fract(vec4(p.xyxy) * vec4(.1031, .1030, .0973, .1099));
				p4 += dot(p4, p4.wzxy+33.33);
				return fract((p4.xxyz+p4.yzzw)*p4.zywx);
			}

			vec4 _hash43(vec3 p){
				vec4 p4 = fract(vec4(p.xyzx)  * vec4(.1031, .1030, .0973, .1099));
				p4 += dot(p4, p4.wzxy+33.33);
				return fract((p4.xxyz+p4.yzzw)*p4.zywx);
			}

			vec4 _hash44(vec4 p4){
				p4 = fract(p4  * vec4(.1031, .1030, .0973, .1099));
				p4 += dot(p4, p4.wzxy+33.33);
				return fract((p4.xxyz+p4.yzzw)*p4.zywx);
			}
		`
		//TODO add iq distance functions
		let distance = `
			float _sdCapsule( vec3 p, vec3 a, vec3 b, float r )
			{
				vec3 pa = p - a, ba = b - a;
				float h = clamp( dot(pa,ba)/dot(ba,ba), 0.0, 1.0 );
				return length( pa - ba*h ) - r;
			}
		`

		header += hash;
		header += distance;
	//== Temporary Texture ========================================
		let tempMap = new Map();

		context.tempTex = function(type,size,f){
			let pattern = den[type].valueOf();

			let sizeLs = tempMap.get(pattern);
			if (!sizeLs) tempMap.set(pattern,sizeLs = new Map());
			let hash   = `[${size.toString()}]`;

			let tempLs = sizeLs.get(hash);
			if (!tempLs) sizeLs.set(hash,tempLs = []);

			let obj = undefined;
			for (let temp of tempLs) if (!temp.active) obj = temp;
			if (!obj) tempLs.push(obj = {
				active  : false,
				texture : den[type](size),
			});

			obj.active = true;
			obj.texture._clear();
			let retVal = f(obj.texture);
			obj.active = false;
			return retVal;
		};
	//== Texture Extensions =======================================
		/* notes:
			 - Frag
			 - automatic type system
			 - passthrough
		*/
		let vertBase = {
			count:4,
			mode:triFan,
			vert : `
				vec2 vtx[4] = vec2[](vec2(-1,-1),vec2(1,-1),vec2(1,1),vec2(-1,1));
				void render () {gl_Position=vec4(vtx[gl_VertexID],0,1);}
			`,
		}
		let Main = (glsl) => {
			return {
				...vertBase,
				frag : `
					${header}
					void render(){
						${glsl}
					}
				`        
			}
		}

		let defaultDefs = {
			_time :float,
			_mouse:float2,
		}
		let getDefaultParams = () => {return {
			_time:performance.now()/1000,
			_mouse:[0, 0], //TODO replace with actual mouse
		}}

		//note must bet passed either vertbase or a vert to run
		Texture.prototype._run = function(defs, params){
			return this.run({
				...defaultDefs,
				...defs, 
			},{
				...getDefaultParams(),
				...params,
			});
		}

		Texture.prototype._frag = function(defs, params){
			defs.frag = header+defs.frag;
			defs.frag = defs.frag.replace("main", "render");
			return this.run({
				...defaultDefs,
				...vertBase,
				...defs, 
			},{
				...getDefaultParams(),
				...params,
			});
		}

		Texture.prototype._io = function(glsl){
			return this._run(
				Main(`
					vec4 i = _thisTexel(image);
					vec4 o = i;
					${glsl};
					outColor = o${this.swizzle};
				`)
			)
		}

		//stolen directly from den.js
		Texture.prototype._lookupIO = function(lookup,defParams,args,vertRender,fragRender,header='') {
			let size     = lookup.size;
			let len      = lookup.channels;
			let processed = {}, alias = ``;
			for (let [name,value] of Object.entries(defParams)) {
				if (Array.isArray(value)) processed[name] = float | den.channels(value.length);
				else if (typeof value === "number") processed[name] = float;
				else {
					// TODO: ALLOW PARALLEL BUFFERS AS WELL
					processed[name] = (value.pattern||value) | input;
					if (value.size[0]*value.size[1]*len == size) 
						alias += `${value.ctor} ${name} = texelFetch(${name},lookup.xy,0)${value.swizzle};\n`;
				}
			}

			return this.run({
				lookup,...processed,...args,
				vert : `void render(){\n${alias}\n${vertRender}\n;}`,
				frag : `${header}void render(){\n${alias}\n${fragRender}\n;}`,
			},{lookup,...defParams});
		},
	//== Texture Utilities ========================================
		Texture.prototype._clear = function(){
			return this._io(`o = vec4(0)`);
		}

		Texture.prototype._setTo = function(tex, options={}){
			return this._frag({
				tex  : tex.pattern | input,
				size : float2,
				...options,
				frag : `
					void render(){
						vec4 i = vec4(0, 0, 0, 1);
						i = texture(tex, pixel/res);
						outColor = i${this.swizzle};
					}
				`
			},{tex, size:tex.size});
		}

		Texture.prototype._hueShift = function(amt){
			return this._frag({
				amt : float,
				frag : `
					void render(){
						vec4 i = _thisTexel(image);
						vec3 col = i.rgb;
						col = _rgb2hsv(col);
						outColor = hsv(col.r + amt, col.g, col.b, i.a);
					}
				`
			},{amt});
		}

		Texture.prototype._xy2hue = function(amt = 1){
			return this._frag({
				amt  : float,
				frag : `
				void main(){
					vec4  i = _thisTexel(image);
					float a = atan(i.y, i.x);
					float d = length(i.xy);

					outColor = hsv(a/tau + .5, 1., d*amt);
				}
			`},{amt});
		}

		Texture.prototype._dXdY = function(){
			return this._frag({
				frag : `
					void render(){
						vec4 i = vec4(0);
						// i.x = _pixelR(image).x - _pixelL(image).x;
						// i.y = _pixelU(image).x - _pixelD(image).x;
						i.x = texture(image, (pixel+vec2(-1, 0))/res).x - texture(image, (pixel+vec2(1, 0))/res).x;
						i.y = texture(image, (pixel+vec2(0, -1))/res).x - texture(image, (pixel+vec2(0, 1))/res).x;

						outColor = i${this.swizzle};
					}
				`
			});
		}

		//TODO fix this to have varying behavior depending on if the src is a vec4
		Texture.prototype._srcOver = function(tex){
			if (this.swizzle !== ".xyzw" && tex.swizzle !== ".xyzw") return this;
			return this._frag({
				tex : this.pattern | input,
				frag : `
					void render(){
						vec4 a = texture(tex  , _uv);
						vec4 b = texture(image, _uv);
						float alpha = a.a + b.a*(1. - a.a);
						outColor = vec4((a.rgb*a.a + b.rgb*b.a*(1. - a.a))/alpha, alpha);
					}
				`
			},{tex});
		}

		Texture.prototype._computeNormals = function(mult=1){
			context.tempTex(`f1tex`, this.size, height => {
				height._setTo(this);
				this._dXdY().io({height, mult}, `
					this *= mult;
					vec3 vx = normalize(vec3(2,0,this.x));
					vec3 vy = normalize(vec3(0,2,this.y));
					this = vec4(cross(vx, vy), height);
				`)
			})
			return this;
		}

		Texture.prototype._glow = function(radius=5, mult=1){
			let type = `f${this.channels}tex`;
			context.tempTex(type, this.size, temp => {
				temp._setTo(this)._blur({radius});
				this.io({temp, mult}, `this = (this + temp)*mult`);
			})
			return this;
		}

		Texture.prototype._median5 = function(){
			return this._frag({
				frag : `
					/*
						3x3 Median

						GLSL 1.0
						Morgan McGuire and Kyle Whitson, 2006
						Williams College
						http://graphics.cs.williams.edu

						Copyright (c) Morgan McGuire and Williams College, 2006
						All rights reserved.

						Redistribution and use in source and binary forms, with or without
						modification, are permitted provided that the following conditions are
						met:

						Redistributions of source code must retain the above copyright notice,
						this list of conditions and the following disclaimer.

						Redistributions in binary form must reproduce the above copyright
						notice, this list of conditions and the following disclaimer in the
						documentation and/or other materials provided with the distribution.

						THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
						"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
						LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
						A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
						HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
						SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
						LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
						DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
						THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
						(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
						OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
					*/

					// Change these 2 defines to change precision
					#define vec vec4
					#define toVec(x) x.rgba

					#define s2(a, b)				                  temp = a; a = min(a, b); b = max(temp, b);
					#define t2(a, b)				                  s2(v[a], v[b]);
					#define t24(a, b, c, d, e, f, g, h)			  t2(a, b); t2(c, d); t2(e, f); t2(g, h); 
					#define t25(a, b, c, d, e, f, g, h, i, j)	t24(a, b, c, d, e, f, g, h); t2(i, j);


					void main() {

						vec v[25];

						for(int dX = -2; dX <= 2; ++dX) {
							for(int dY = -2; dY <= 2; ++dY) {
								v[(dX + 2) * 5 + (dY + 2)] = toVec(_pixelOffset(image, dX, dY));
							}
						}

						vec temp;

						t25( 0,  1,	  3,  4,	 2,  4,	  2,  3,   6,  7);
						t25( 5,  7,	  5,  6,	 9,  7,	  1,  7,   1,  4);
						t25(12, 13,	 11, 13,	11, 12,	 15, 16,  14, 16);
						t25(14, 15,	 18, 19,	17, 19,	 17, 18,  21, 22);
						t25(20, 22,	 20, 21,	23, 24,	  2,  5,   3,  6);
						t25( 0,  6,	  0,  3,	 4,  7,	  1,  7,   1,  4);
						t25(11, 14,	  8, 14,	 8, 11,	 12, 15,   9, 15);
						t25( 9, 12,	 13, 16,	10, 16,	 10, 13,  20, 23);
						t25(17, 23,	 17, 20,	21, 24,	 18, 24,  18, 21);
						t25(19, 22,	  8, 17,	 9, 18,	  0, 18,   0,  9);
						t25(10, 19,	  1, 19,	 1, 10,	 11, 20,   2, 20);
						t25( 2, 11,	 12, 21,	 3, 21,	  3, 12,  13, 22);
						t25( 4, 22,	  4, 13,	14, 23,	  5, 23,   5, 14);
						t25(15, 24,	  6, 24,	 6, 15,	  7, 16,   7, 19);
						t25( 3, 11,	  5, 17,	11, 17,	  9, 17,   4, 10);
						t25( 6, 12,	  7, 14,	 4,  6,	  4,  7,  12, 14);
						t25(10, 14,	  6,  7,	10, 12,	  6, 10,   6, 17);
						t25(12, 17,	  7, 17,	 7, 10,	 12, 18,   7, 12);
						t24(10, 18,	 12, 20,	10, 20,	 10, 12);
						toVec(outColor) = v[12];
					}




					//float lessThan(float a, float b) {
					//	return a < b;
					//}

					// Swap so that a < b.  If b > a, then delta is > 0.  In this case, test = 0 and no swap occurs
					//#define s2(a, b)				delta = b - a; test = vec(lessThan(delta, ZERO)); a += delta * test;  b -= delta * test;
					//#define s2DropLo(a, b)			delta = b - a; test = vec(lessThan(delta, ZERO)); b -= delta * test;
					//#define s2DropHi(a, b)			delta = b - a; test = vec(lessThan(delta, ZERO)); a += delta * test;

					/*
						// Optional :Expand out the last step to dump the result directly into gl_FragColor
						mx3(v[3], v[4], v[8]);
						delta = v[4] - v[3];
						test = vec3(lessThan(delta, ZERO))
						gl_FragColor = b - delta * test;*/
				`
			},{})
		}

		Texture.prototype._normalize = function(){
			let stats = this._fullStats();
			let mins = [];
			let maxs = [];
			let c = this.channels;
			for (let i = 0; i < 4; i++){
				if (i < c){
					mins[i] = stats[i].min;
					maxs[i] = stats[i].max;
				} else {
					mins[i] = 0;
					maxs[i] = 1;
				}
			}
			return this._frag({
				mins : float4,
				maxs : float4,
				frag : `
					void render(){
						vec4 i = _thisTexel(image);
						i = (i-mins)/(maxs-mins);
						outColor = i${this.swizzle};
					}
				`
			},{mins, maxs});
		}
	//== Texture toPNG ============================================
		Texture.prototype._toPNG = function(src){
			context.tempTex(`f${this.channels}tex`,this.size,rgba=>{
				rgba._setTo(this);
				let data = rgba.getData();
				let cvs = den.c2tex(this.size);
				let data255 = [];
				for (let i = 0; i < data.length; i++) data255[i] = data[i]*255;
				let d = new Uint8ClampedArray(data255)
				cvs.c2d.context.putImageData(new ImageData(d,...this.size),0,0);
				cvs.c2d.canvas.toBlob(blob=>{
					let reader = new FileReader();
					reader.onload = () => {
						try{
							let buffer = new window.Buffer(reader.result);
							fs.writeFileSync(src+".png",buffer);
						} catch(err){
							console.log(err);
						}
					};
					reader.readAsArrayBuffer(blob);
				});
			});
			return this;
		};
	//== Texture Statistics =======================================
		/* notes:
			 - min, max, avg, stdev
			 - min, max, total, sqrtotal
			 - per specified area / direction
			 - statX, statY (x, y, w, h) [stripStatX, stripStatY]
				 - record [min, max, avg, stdev] 
					 into   [r,   g,   b,   a     ]
			 - fullStat = statX(statY(tex))
		*/

		Texture.prototype._dirStat = function({dir=0, x=0, y=0, w=this.size[0], h=this.size[1]}){
			let channels = this.channels;
			let size = dir == 0 ? [w, channels] : [channels, h];
			let type = `f${this.channels}tex`;
			let rtex = undefined;
			let len  = dir==0 ? h : w;
			context.tempTex("f4tex", size, tex => {
				rtex = tex;
				tex._frag({
					src      : den[type] | input,
					size     : float2,
					len      : float,
					viewPort : den.int4,
					dir      : den.int,
					frag : `
						vec2 dirs[2] = vec2[2](vec2(1, 0), vec2(0, 1));
						void render(){
							int   idx   = texel[(dir+1)%2];
							vec2  p1    = vec2(viewPort.xy) + dirs[dir]*pixel[dir];
							float t     = texelFetch(src, ivec2(p1), 0)[idx];
							vec4  stats = vec4(t, t, t, t*t);
							for (float i = 1.; i < len; i++){
								vec2 p2 = p1 + i*dirs[(dir+1)%2];
								t = texelFetch(src, ivec2(p2), 0)[idx];
								stats.x = min(stats.x, t);
								stats.y = max(stats.y, t);
								stats.z += t;
								stats.w += t*t;
							}
							// stats.z /= len;
							// stats.w = sqrt((stats.w - len*stats.z*stats.z)/(len-1));
							outColor = stats;
						}
					`
				},{src:this, size:this.size, len, viewPort:[x, y, w, h], dir})
			})
			return rtex;
		}

		Texture.prototype._statStripX = function(x=0, y=0, w=this.size[0], h=this.size[1]){return this._dirStat({dir:0, x, y, w, h})}
		Texture.prototype._statStripY = function(x=0, y=0, w=this.size[0], h=this.size[1]){return this._dirStat({dir:1, x, y, w, h})}

		//TODO - fix the indexing fluxes
		Texture.prototype._fullStats  = function(x=0, y=0, w=this.size[0], h=this.size[1]){
			let ystats = this._statStripY(x, y, w, h);
			let size   = [ystats.size[0]*4, ystats.size[1]]
			let stats  = [];

			// console.log(ystats.getData());

			context.tempTex("f4tex", size, ySpread => {
				ySpread._frag({
					ystats : f4tex | input,
					frag : `
						void render(){
							outColor = vec4(texelFetch(ystats, ivec2(texel.x/4, texel.y), 0)[texel.x%4]);
						}
					`
				},{ystats})


				let pixelCount = w*h;
				let xstats  = ySpread._statStripX();
				let data    = xstats.getData();
				let xlen    = data.length/ystats.size[0];
				let xSublen = 16;
				for (let col = 0; col < ystats.size[0]; col++){
					let s = {min:0, max:0, total:0, sqTotal:0, average:0, stddev:0};
					let x = col*(xlen + xSublen);
					s.min     = data[x];
					s.max     = data[x+5 ];
					s.total   = data[x+10];
					s.sqTotal = data[x+14];
					s.average = s.total/(pixelCount);
					s.stddev  = Math.sqrt((s.sqTotal - pixelCount*s.average*s.average)/(pixelCount-1));
					stats.push(s);
				}
			})
			return stats;
		}
	//== Texture Neighborhood =====================================
		/* notes:
			 - accepts glsl op
			 - performs it in for loop with kernal mask
			 - generate kernals from ops
		*/
	//== Blur =====================================================
		/* notes:
			 - directionalBlur
			 - basicBlur
			 - mapBlur
		*/
		// Texture.prototype.initBlur = function(downSample = 3)

		Texture.prototype._dirBlur = function({radius, wrap, dir, texture=false}){
			return this.frag({
				radius : (texture ? f1tex : float),
				dir    : den.int,
				frag : `
					vec2 dirs[2] = vec2[2](vec2(1, 0), vec2(0, 1));
					void render(){
						${this.ctor} img = texture(image, pixel/res)${this.swizzle};
						float total = 1.;
						for (float i = 1.; i <= radius; i += 1.){
							float amt = i/radius;
							float a = cos(amt*pi) + 1.;
							// a /= 2.;
							img += texture(image, (pixel + dirs[dir]*i)/res)${this.swizzle}*a/2.;
							img += texture(image, (pixel - dirs[dir]*i)/res)${this.swizzle}*a/2.;
							total += a;
						}
						outColor = img/total;
					}
				`
			},{radius, dir, wrap});
		}

		Texture.prototype._blur = function({radius=1, downSamples=Math.floor(Math.max(Math.log2(radius), 3)-3), wrap = den.xyzClamp, quality=1}){
			if (typeof arguments[0] !== "object"){
				radius  = arguments[0];
				wrap    = arguments[1];
				quality = 1;
			}
			
			let f = (tex, radius) => {
				tex._dirBlur({radius, wrap, dir:0});
				tex._dirBlur({radius, wrap, dir:1});
				this._setTo(tex);
			}
			if (downSamples > 0){
				let exp  = 2**downSamples;
				let r    = Math.floor(radius/exp);
				let size = this.size.map(i => Math.floor(i/exp));
				let type = `f${this.channels}tex`;

				context.tempTex(type, size, tex => {
					//do an average downsample first (nearest neighbor)
					tex._frag({
						src : den[type] | input,
						exp : float,
						size : float2,
						frag : `
							void render(){
								vec4 o = vec4(0);
								vec2 p = pixel*exp;
								float total = 0.;
								for (float i = 0.; i < exp; i++){
									for (float j = 0.; j < exp; j++){
										o += texture(src, (p + vec2(i, j))/size);
										total += 1.;
									}
								}
								outColor = (o/(exp*exp))${this.swizzle};
							}
						`
					},{src:this, size:this.size, exp})
					f(tex, r);
				})
			} else {
				f(this, radius);
			}
			return this;
		}

		Texture.prototype._octaveBlur = function({radius=1, octaves = 5, downSamples=1, wrap = den.xyzClamp, base=2}){
			let type = `f${this.channels}tex`;
			let total = 0;
			context.tempTex(type, this.size, acc => {
				context.tempTex(type, this.size, buf => {
					for (let i = 0; i < octaves; i++){
						let exp = Math.pow(base, i);
						total += 1/exp;
						let r   = radius/exp;
						buf._setTo(this)._blur({radius:r, wrap});
						acc.io({buf, exp}, `this += buf/exp`);
					}
				})
				acc.io({total},`this /= total`);
				this._setTo(acc);
			})
			return this;
		}

		Texture.prototype._contextBlur = function({radius, downSamples=1, wrap=den.xyzClamp, incr=1}){
			if (typeof arguments[0] !== "object" || arguments[0].size != undefined){
				radius = arguments[0];
				wrap   = arguments[1];
				incr   = 1;
			}
			this._dirBlur({radius, wrap, dir:0, texture:true});
			this._dirBlur({radius, wrap, dir:1, texture:true});
			return this;
		}
	//== Advection / Fluid ========================================
		Texture.prototype._advect = function(mag = 1, vel = this, exp=1){
			return this.frag({
				vel : this.pattern | input,
				mag : float,
				exp : float,
				frag : `
					void render(){
						vec2 v = texture(vel, pixel/res).xy*mag;
						float d = length(v);
						if (d > 0.) v = normalize(v)*pow(d, exp);
						outColor = texture(image, (pixel-v)/res)${this.swizzle};
					}
				`
			},{vel, mag, exp});
			// return this.frag({vel, mag}, `this = texture(image, (pixel+vel*mag)/res)${this.swizzle}`)
		}

		//apply temporary textures f1tex for curl
		Texture.prototype._stepFluid = function(motion, curl, dt = 1, curlMag = 80, fade=.99){return this._fluid({motion, dt, curlMag, fade})}

		Texture.prototype._fluid = function({motion, dt = 1, curlMag = 80, fade=.99, blurRadius=10, curlRadius=20, mult=1}){
			let dt0 = dt*Math.max(this.size[0], this.size[1])
			this.io({motion, fade, mult}, `this = (this + motion.xy*mult)*fade`);
			this._blur({radius:blurRadius});
			this._advect(dt0);

			context.tempTex("f1tex", this.size, curl => {
				curl._frag({
					velocity : f2tex | input,
					frag : `
						void main(){
							outColor = -.5*(_texelR(velocity).x - _texelL(velocity).x + 
															_texelU(velocity).y - _texelD(velocity).y);
						}
					`
				},{velocity:this});
				curl._blur({radius:curlRadius});
				this._frag({
					curl    : f1tex | input,
					curlMag : float,
					frag : `
						void render(){
							outColor = _thisTexel(image).xy - vec2(_texelR(curl).x - _texelL(curl).x,
																										 _texelU(curl).x - _texelD(curl).x)*curlMag;
						}
					`
				},{curl, curlMag})
			})
		}
	//== localNorm ================================================
		Texture.prototype._localNorm = function(radius = 1){
			return this._frag({
				radius : den.int,
				frag : `
					void render(){
						float m = 1e5;
						float x = -1e5;
						float n = _thisTexel(image).x;
						forChebyshev(i, j, radius){
							vec2  offset = vec2(i, j);
							if (length(offset) <= float(radius)){
								float v = _pixelOffset(image, offset.x, offset.y).x;
								if (v < m) m = v;
								if (v > x) x = v;
							}
						}

						n = (n-m)/(x-m);
						outColor = vec4(n);
					}
				`
			},{radius})
		}
	//== Noise ====================================================
		/* notes:
			 - per texure
			 - cpu seeded
			 - blur algorithm
			 - TODO periodicity
		*/
		Texture.prototype._noiseParams = {
			seed  : Math.random()*1e9,
			seek  : 10000,
			speed : .01,
		}

		Texture.prototype._noise = function(radius = 1, wrap=den.xyzRepeat){
			if (typeof arguments[0] === "object"){
				if (arguments[0].radius != undefined) radius = arguments[0].radius;
				if (arguments[0].warp   != undefined) warp   = arguments[0].warp;
			}
			this._frag({
				seed : float,
				seek : float,
				frag : `
					void render(){
						outColor = abs(mod(_hash42(pixel + _hash21(seed)*10000.)*seek, vec4(2)) - 1.)${this.swizzle};
					}
				`
			},{...(this._noiseParams)});
			this._blur(radius, wrap);
			this._frag({//noramlize the noise 0-1
				radius : float,
				frag : `
					void render(){
						float off = .5 - 1./radius;
						outColor = ((_thisTexel(image)-off)*radius/2.)${this.swizzle};
					}
				`
			},{radius})
			this._noiseParams.seek += this._noiseParams.speed;
			return this;
		}

		Texture.prototype._octaveNoise = function({radius = 1, wrap=den.xyzRepeat, octaves=5, base=2}){
			let type = `f${this.channels}tex`;
			let total = 0;
			context.tempTex(type, this.size, acc => {
				for (let i = 0; i < octaves; i++){
					let exp = Math.pow(base, i);
					total += 1/exp;
					let r   = radius/exp;
					this._noise(r, wrap);
					this._noiseParams.seek -= this._noiseParams.speed;
					acc.io({buf:this, exp}, `this += buf/exp`);
				}
				acc.io({total},`this /= total`);
				this._setTo(acc);
			})
			this._noiseParams.seek += this._noiseParams.speed;
			return this;
		}
	//== Mouse ====================================================
		context.mouse  = {x:0, y:0};
		context.pmouse = {x:0, y:0};
		context.getMouse = () => [context.mouse.x, context.mouse.y];
		window.addEventListener("mousemove", evt => {
			context.mouse = {x:evt.clientX/window.innerWidth, y:evt.clientY/window.innerHeight};
		})
	//== Simulated Depth/Motion ===================================
		context.updateMotion = function({motion, depth, radius=50, mult=1}){
			let mouse  = [context.mouse.x , context.mouse.y ];
			let pmouse = [context.pmouse.x, context.pmouse.y];

			if (motion != undefined) motion._frag({
				mouse  : float2,
				pmouse : float2,
				radius : float,
				mult   : float,
				frag : `
					void render(){
						vec4 i = vec4(0.);
						float d = _sdCapsule(vec3(pixel, 0), vec3(mouse*res, 0), vec3(pmouse*res, 0), radius);
						if (d < 0. && length(mouse-pmouse) > 0.){
							i.xy = normalize(mouse-pmouse)*(-d/radius)*mult*.5;
							i.b = 1.;
						}
						outColor = i${motion.swizzle};
					}
				`
			},{mouse, pmouse, radius, mult})

			if (depth != undefined) depth._frag({
				mouse  : float2,
				radius : float,
				frag : `
					void main(){
						outColor = (length(pixel-mouse*res) < radius) ? 1. : 0.;
					}
				`
			},{mouse, radius})
			
			context.pmouse = {...(context.mouse)};
		}
	//== Motion ===================================================
		/*
			obj contains:
				size = [,]
				surfaceName
				let motion     = f2tex(size);
				let motionAcc  = f2tex(size);
				let depth      = f1tex(size);
				let prevMotion = f2tex(size);
				let dist       = f2tex(size);
				let dirThresh  = -0.5;
				let magThresh  = 50;

				let radius     = 30

				let flow       = f2tex(size);
				let mult       = 1;
				let flowMult   = 10;
				let dt         = .9;
				let curlMag    = 80;
				let fade       = .95;
		*/
		context.motion = function(obj){
			if (!obj.size) return;
			let inject = (name, type) => {if (!obj[name]) obj[name] = den[type](obj.size);}

			inject("motion", "f2tex");
			inject("depth" , "f1tex");
			inject("flow"  , "f2tex");
			if (typeof Kinect === "undefined") context.updateMotion(obj);
			else {
				inject("motionAcc" , "f2tex");
				inject("prevMotion", "f2tex");
				inject("dist"      , "f2tex");
				let {dirThresh = -0.5, magThresh = 50} = obj;

				Kinect.getConvertedDepth(den,obj.depth,obj.surfaceName);

				den.FragIO({depth:obj.depth,prev:obj.prevMotion,dist:obj.dist,motion:obj.motion,dirThresh,magThresh},`
					float n = dot(normalize(prev),normalize(dist));
					motion = n>dirThresh || length(dist-prev)>magThresh ? float2(0) : -float2(dist-prev);
					prev = dist;
					dist = float2(depth==0.0);
				`);

				obj.dist.edt();
				obj.motionAcc.io({motion:obj.motion},`
					this*=0.7;
					this-=motion*0.3;
				`)._blur({radius:10});
			}
			let {flowMult = 1, dt = .9, curlMag = 80, fade = .95} = obj;
			if (typeof Kinect !== "undefined") obj.motion._setTo(obj.motionAcc);
			obj.flow._fluid({...obj, mult:obj.flowMult});
			return obj;
		}
	//== Particles ================================================
		context.Particles = class{
			constructor(count, size){
				this.count  = count.map(i => Math.floor(i));
				this.size   = size;
				this.coordPoints = i2buf(this.count, i => i);
				this.coordLines  = i3buf(this.count, i => [...i, 0, ...i, 1]);
				this.attrs  = {size: size};
				this.defs   = {size: float2};
				this.protoTex("init"  , "f2tex", i => size.map(j => Math.random()*j));
				this.protoTex("pos"   , "f2tex")._setTo(this.init);
				this.protoTex("prev"  , "f2tex")._setTo(this.init);
				this.protoTex("vel"   , "f2tex", i => [0,0]);
			}
			reset(){
				this.pos._setTo(this.init);
				this.vel._io(`o = vec4(0)`);
			}
			protoTex(name, type, init = i => i){//todo fix size arraybuffer issue for non f2tex
				this[name] = den[type](this.count, init);
				this.defs[name] = den[type];
				this.attrs[name] = this[name];
				return this[name];
			}
			proTex(name, type, init = i => i){return this.protoTex(name, type, init)}
			frag(defs, attrs={}){
				den.Frag({
					...this.defs,
					...defs
				},{
					...this.attrs,
					...attrs
				})
			}
			render({src, pointSize=2, blend=den.srcOnly, samplers={}, header='', vert = `
				gl_Position  = vec4(pos*2./res - 1., 0, 1);
				gl_PointSize = ${pointSize}.;
			`, frag = `
				outColor = vec4(1)${src.swizzle};
			`}){
				return src._lookupIO(this.coordPoints, {...this.attrs,...samplers}, {mode:den.points, wrap:den.xyzRepeat, blend}, vert, frag, context.header + header);
			}
			renderLines({src, blend=den.srcOnly, header='', vert = `
				gl_Position  = vec4((lookup.z == 0 ? prev : pos)*2./res - 1., 0, 1);
			`, frag = `
				outColor = vec4(1);
			`}){
				return src.lookupIO(this.coordLines, this.attrs, {mode:den.lines, blend}, vert, frag, context.header+header);
			}
		}
	//== Spritesheets =============================================
		context.createSpritesheet = function(arr){//needs to be f4tex images and not canvas2D images
			let images = [...arr];
			images.map((e,i) => e.idx = i);
			let imgs = images.sort((a, b) => {
				if (a.size[1] == b.size[1]) return b.size[0]-a.size[0];
				return b.size[1]-a.size[1];
			});
			let totalX = 0;
			for (let img of imgs) totalX += img.size[0];
			
			let goalWidth = Math.ceil(totalX/Math.floor(Math.sqrt(imgs.length)));
			let width  = 0;
			let height = 0;

			let imgMap = (new Array(images.length)).fill([]);
			let x = 0;
			let y = 0;
			let curRowHeight = 0;

			for (let img of imgs){
				let x2 = x + img.size[0];
				curRowHeight = Math.max(curRowHeight, img.size[1]);
				height = Math.max(height, y + curRowHeight);

				imgMap[img.idx] = [x, y, ...img.size];

				if (x2 > goalWidth){
					width = Math.max(width, x2);
					x = 0;
					y += curRowHeight;
					curRowHeight = 0;
				} else {
					x = x2;
				}
			}

			if (width > 16384 || height > 16384){ //TODO - fix hardcoded values
				console.warn("Too many images to create spritesheet: Pixel dimensions beyond 16384");
				return;
			}

			let tex = f4tex([width, height]);
			for (let img of imgs){
				let bounds = imgMap[img.idx];
				tex._frag({
					img : f4tex | input,
					bounds : float4,
					frag : `
						void main(){
							outColor = _thisTexel(image);
							if (pixel.x >= bounds.x && pixel.y >= bounds.y && pixel.x < bounds.x + bounds.z && pixel.y < bounds.y + bounds.a){
								outColor = texelFetch(img, ivec2(pixel-bounds.xy), 0);
							}
						}
					`
				},{img, bounds});
			}

			return [tex, imgMap];
		}
	//== Collision ================================================
		context.getCollisionMap = function(size=[101, 101], inner=1.){
			let collisionMap = f2tex(size).io({inner},`
				vec2 p = pixel-res/2.;
				float len = length(p);
				float amt = len/(res.x/2.);
				vec2 v = normalize(p)*(1. - amt);
				if (amt > 1. || amt == 0. || len < inner) v = vec2(0);
				this = v*vec2(1,-1);
			`)
			return collisionMap;
		}
		context.getCollider = context.getCollisionMap;
	//== Parameters ===============================================
		/* notes:
			- tied to the visual
			- don't clog the dom
			- rendered to canvas
			- save to saves.json
			- modes
		*/

		let getLabel = (name) => {
			let label = document.createElement("label");
			label.innerHTML = name;
			return label;
		}
		let getOption = (name) => {
			let option = document.createElement("option");
			option.value = name;
			option.innerHTML = name;
			return option;
		}

		let getInput = () => {return document.createElement("input")}

		let getSlider = () => {
			let elem  = getInput();
			elem.type = "range";
			return elem;
		}

		let createControls = () => {
			let controls = document.createElement("div");
			controls.id = "_controls";
			controls.style = `
				position: absolute;
				top: 0; left: 0;
				padding: 3px 5px;
				color:white;
				font-family: "Lucida Console";
				z-index:2;

				display : none;
				grid-template-columns: repeat(3, min-content);
				column-gap : 10px;
				row-gap : 5px;

				background: rgba(0, 0, 0, .7);
				user-select: none;
			`;
			return controls;
		}

		let addControlElem = () => {
			let controls = document.getElementById("_controls");
			if (controls === null){
				controls = createControls();
				document.body.appendChild(controls);
			}
			while (controls.lastElementChild) controls.removeChild(controls.lastElementChild);
			return controls;
		}

		class Control{
			constructor(args, obj){
				let defaults = {value:0, min:0, max:1, step:.01};
				Object.assign(defaults, args);

				this.name   = args.name;
				this.obj    = obj;
				this.label  = getLabel(this.name + ":");
				this.slider = getSlider();
				this.slider.style = "width:200px;";
				this.input  = getInput();
				this.label.style = "place-self: center end;"
				this.input.type = "number";
				Object.assign(this.input , args);
				Object.assign(this.slider, args);

				this.setValue(args.value, false);

				this.slider.oninput = () => {this.setValue(parseFloat(this.slider.value))};
				this.input .oninput = () => {this.setValue(parseFloat(this.input .value))};
				obj.values[this.name] = defaults.value;
			}
			setValue(val, change=true){
				if (val === "NaN" || isNaN(val)) return;
				// console.log(val);
				this.input.value    = val;
				this.slider.value   = val;
				this.obj.values[this.name] = val;
				this.obj.getSelected()[this.name] = val;
				if (change) this.obj.onChange();
			}
		}

		context.addControls = function(...args){
			let obj = {values:{}, sets:{_default:{selected:true}}, defaultEnabled:false};
			let internalControls = {};
			obj.setValues = json => {
				for (let key of Object.keys(json)){
					if (internalControls[key] != undefined) internalControls[key].setValue(json[key], false);
				}
				obj.onChange();
			}
			obj.getSelected = () => obj.sets[obj.select.value];
			obj.onChange    = () => {};
			obj.onLoad      = () => {};
			obj.showControls = () => {if (obj.controls != undefined) obj.controls.style.display = "grid";}
			obj.hideControls = () => {if (obj.controls != undefined) obj.controls.style.display = "none";}
			obj.toggleControls = () => {
				if (obj.controls.style.display === "grid") obj.hideControls();
				else obj.showControls();
			}
			obj.save        = path => {context.saveJSON(obj.sets, path)};
			obj.load        = path => {
				obj.path = path;
				let json = context.readJSON(path);
				Object.assign(obj.sets, json);
				let keys = Object.keys(json).sort();

				for (let key of keys){
					if (key !== "_default") obj.select.appendChild(getOption(key));
					if (json[key].selected){
						obj.select.value = key;
						obj.setValues(json[key]);
					}
				}
			}
			obj.enableDefault = path => {
				obj.onLoad   = () => {obj.load(path), obj.showControls()};
				obj.onChange = () => {obj.save(path)}
				obj.defaultEnabled = true;
				return obj;
			}

			window.addEventListener('load', () => {
				window.addEventListener("keypress", evt => {if (evt.key === "`") obj.toggleControls()});

				let controls = addControlElem(); 
				obj.controls = controls;
				obj.select = document.createElement("select");
				obj.select.appendChild(getOption("_default"));
				obj.select.onchange = () => {
					for (let s in obj.sets) obj.sets[s].selected = false;
					obj.setValues(obj.getSelected());
					obj.curSelected = obj.getSelected();
					obj.curSelected.selected = true;
					if (obj.defaultEnabled) obj.save(obj.path);
				};
				let input = getInput();
				input.onfocus = () => {input.value = ''}
				input.addEventListener("keyup", evt => {
					if (evt.key === "Enter" && input.value.length > 0){
						console.log(input.value);
						obj.select.appendChild(getOption(input.value));
						obj.sets[input.value] = {...obj.values};
						obj.select.value = input.value;
						obj.setValues(obj.getSelected());
						input.value = "";
						obj.select.onchange();
					}
				});

				controls.appendChild(obj.select);
				controls.appendChild(input);
				controls.appendChild(getLabel(""));

				for (let i = 0; i < args.length; i++){
					let control = new Control(args[i], obj);
					internalControls[args[i].name] = control;
					controls.appendChild(control.label);
					controls.appendChild(control.slider);
					controls.appendChild(control.input);
				}
				obj.onLoad();
			})
			return obj;
		}
	//== Video Recorder ===========================================
		/*
			- file path
			- hotkey
			- indicator (shape)
		*/

		//see https://stackoverflow.com/questions/39874867/canvas-recording-using-capturestream-and-mediarecorder
		//and https://stackoverflow.com/questions/40137880/save-video-blob-to-filesystem-electron-node-js
		
		let saveDir = "./recordings"
		context.getRecorder = function(name){
			let canvas = document.querySelector('canvas');
			let chunks = [];

			let recorder = new MediaRecorder(canvas.captureStream(30), {
				videoBitsPerSecond : 6000000,
				mimeType: 'video/webm; codecs="vp9"'
			});

			recorder.ondataavailable = (evt) => {
				if (evt.data.size > 0){
					chunks.push(evt.data);
				}
			};
			recorder.onstop = () => {
				let blob = new Blob(chunks);
				let reader = new FileReader();

				if (!fs.existsSync(saveDir)) fs.mkdirSync(saveDir);

				reader.onload = function(){
					let date = new Date();
					let dateStr = "_"+(date.getMonth()+1)+"."+date.getDate() + "_" + date.getHours() + "." + date.getMinutes() + "." + date.getSeconds();
					console.log(dateStr);
					let path = `/${name + dateStr}.webm`;

					let buffer = new defaultBuffer(reader.result)
					fs.writeFile(saveDir + path, buffer, {}, (err, res) => {
						if(err) console.error(err)
						else    console.log('video saved');
					})
				}
				reader.readAsArrayBuffer(blob);
				chunks = [];
			};
			return recorder;
		}

		let createRecorderShape = () => {
			// if (document.getElementById("record_symbol") == null) return;
			let div = document.createElement("div");
			div.id = "record_symbol";
			div.innerHTML = `
			<svg height="50" width="50">
				<circle cx="25" cy="25" r="15" stroke="black" stroke-width="1" fill="red" />
			</svg>`;
			div.style = `
				z-index  : 3;
				position : absolute;
				display  : block;
				top      : 10px;
				right    : 10px;
			`
			return div;
		}

		let recorder;
		let recording = false;
		context.record = function({name="",key="`"}){
			if (recorder != undefined){
				console.log("recorder already exists");
				return;
			}
			let div = createRecorderShape();
			window.addEventListener("keyup", evt => {if (evt.key === key){
				if (!recording){
					console.log(`recording ${name}.webm`);
					if (recorder == undefined) recorder = context.getRecorder(name);
					recorder.start();
					document.body.appendChild(div);
					recording = true;
				} else {
					console.log(`writing ${name}.webm`);
					if (recorder != undefined) recorder.stop();
					document.body.removeChild(div);
					recording = false;
				}
			}})
		}
		context.recordWith = context.record;
	return context;
}